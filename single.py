import string
import whois
import time
import datetime
import pandas as pd

symbols = string.digits + string.ascii_lowercase
SLEEP_TIME = 0.9

# check for unregistered domains
table = dict()
for first_symbol in symbols:
    domain = first_symbol+'.am'
    result = whois.query(domain)
    if result is None:
        table[domain] = datetime.datetime(2100, 1, 23)
    else:
        table[domain] = result.expiration_date
    time.sleep(SLEEP_TIME)

df = pd.DataFrame({"expiration": pd.Series(table.values(), index = table.keys()) })
df = df.sort_values(by=["expiration"])

filename = 'single.html'
with open(filename, "w") as f:
    f.write("""<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="sap.am">
    <title>sap.am</title>
</head>
<body>
""")
    for index, row in df.iterrows():
        f.write('<a href="http://'+index+'">'+index+'</a>&nbsp;&nbsp;<a href="https://name.am/search/'+index+'">'+row['expiration'].strftime("%Y-%m-%d")+"</a><br/>")
    f.write("""</body>
</html>
""")
